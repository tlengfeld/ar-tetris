using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    public void SwitchScene(String name)
    {
        SceneManager.LoadScene(name);
    }
}
